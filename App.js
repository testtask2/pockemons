import 'react-native-gesture-handler';
import React from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import RootNavigation from './src/navigation/RootNavigation';
import { MainStoreContext, mainStore } from './src/store';

const App = () => {
    return (
        <MainStoreContext.Provider value={mainStore}>
            <SafeAreaView style={styles.mainView}>
                <RootNavigation />
            </SafeAreaView>
        </MainStoreContext.Provider>
    );
};

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
    },
    scrollView: {
        //backgroundColor: Colors.lighter,
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        //backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        //color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        //color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        //color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },
});

export default App;
