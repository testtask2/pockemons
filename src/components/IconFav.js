import React from 'react';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const IconFav = ({ name, isFavorite, addFavorite, removeFavorite }) => {
    return (
        <View>
            {isFavorite ? (
                <Icon
                    name="favorite"
                    size={30}
                    color="red"
                    onPress={() => removeFavorite(name)}
                />
            ) : (
                <Icon
                    name="favorite"
                    size={30}
                    color="white"
                    onPress={() => addFavorite(name)}
                />
            )}
        </View>
    );
};

export default IconFav;
