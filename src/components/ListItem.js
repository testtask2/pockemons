import React from 'react';
import { TouchableOpacity, View, Image, Text, StyleSheet } from 'react-native';
import { API_URL } from '../const';
import IconFav from './IconFav';

const ListItem = ({
    item,
    toDetailScreen,
    isFavorite,
    addFavorite,
    removeFavorite,
}) => {
    return (
        <TouchableOpacity onPress={() => toDetailScreen(item)}>
            <View style={styles.listItem}>
                <Image
                    style={styles.listImage}
                    source={{ uri: `${API_URL}${item.picture}` }}
                />

                <View style={styles.listViewText}>
                    <Text style={styles.listText}>{item.name}</Text>
                </View>

                <IconFav
                    name={item.name}
                    isFavorite={isFavorite}
                    addFavorite={addFavorite}
                    removeFavorite={removeFavorite}
                />
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    listItem: {
        flex: 1,
        marginRight: 10,
        marginLeft: 10,
        marginTop: 10,
        backgroundColor: '#776677',
        padding: 10,
        borderRadius: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    listImage: {
        height: 50,
        width: 50,
    },
    listViewText: {
        marginLeft: 20,
    },
    listText: {
        color: 'white',
    },
});

export default ListItem;
