import React from 'react';
import { View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Octicons';

const NavDrawer = ({ navigation }) => {
    return (
        <View>
            <TouchableOpacity>
                <Icon name="three-bars" size={30} onPress={() => navigation.toggleDrawer()}  />
            </TouchableOpacity>
        </View>
    );
};

export default NavDrawer;
