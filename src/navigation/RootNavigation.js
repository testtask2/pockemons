import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeScreen from '../screens/HomeScreen';
import DetailScreen from '../screens/DetailScreen';
import FavScreen from '../screens/FavScreen';
import Icon from 'react-native-vector-icons/MaterialIcons';
import NavDrawer from '../components/NavDrawer';

const HomeStack = createStackNavigator();
const HomeStackScreen = () => {
    return (
        <HomeStack.Navigator
            initialRouteName={'Home'}
            screenOptions={{
                headerStyle: { backgroundColor: 'gray' },
                headerRightContainerStyle: {
                    paddingRight: 10,
                },
            }}
        >
            <HomeStack.Screen
                name="Home"
                component={HomeScreen}
                options={({ navigation }) => ({
                    headerLeftContainerStyle: {
                        paddingLeft: 10,
                    },
                    headerLeft: () => (
                        <NavDrawer navigation={navigation} />
                    ),
                })}
            />
            <HomeStack.Screen name="Detail" component={DetailScreen} />
        </HomeStack.Navigator>
    );
};

const FavStack = createStackNavigator();
const FavStackScreen = () => {
    return (
        <FavStack.Navigator
            initialRouteName={'Favorite'}
            screenOptions={{
                headerStyle: { backgroundColor: 'gray' },
                headerRightContainerStyle: {
                    paddingRight: 10,
                },
            }}
        >
            <FavStack.Screen
                name="Favorite"
                component={FavScreen}
                options={({ navigation }) => ({
                    headerLeftContainerStyle: {
                        paddingLeft: 10,
                    },
                    headerLeft: () => (
                        <NavDrawer navigation={navigation} />
                    ),
                })}
            />
            <FavStack.Screen name="Detail" component={DetailScreen} />
        </FavStack.Navigator>
    );
};

const DrawerStack = createDrawerNavigator();
const DrawerStackScreen = () => {
    return (
        <DrawerStack.Navigator initialRouteName="Home">
            <DrawerStack.Screen
                name="Home"
                component={HomeStackScreen}
                options={{
                    drawerIcon: ({ size }) => (
                        <Icon name="home" size={size} color="#ccc" />
                    ),
                }}
            />
            <DrawerStack.Screen
                name="Favorite"
                component={FavStackScreen}
                options={{
                    drawerIcon: ({ size }) => (
                        <Icon name="favorite" size={size} color="#ccc" />
                    ),
                }}
            />
        </DrawerStack.Navigator>
    );
};

const RootNavigation = () => {
    return (
        <NavigationContainer>
            <DrawerStackScreen />
        </NavigationContainer>
    );
};

export default RootNavigation;
