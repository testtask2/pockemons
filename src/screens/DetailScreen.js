import React, { useContext, useLayoutEffect } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import IconFav from '../components/IconFav';
import { MainStoreContext } from '../store';
import { observer } from 'mobx-react';
import { API_URL } from '../const';

const DetailScreen = ({ route, navigation }) => {
    const { item } = route.params;
    const store = useContext(MainStoreContext);

    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <IconFav
                    name={item.name}
                    isFavorite={store.isFavorite(item.name)}
                    addFavorite={store.addFavorite}
                    removeFavorite={store.removeFavorite}
                />
            ),
        });
    }, [navigation, store.favorites.slice()]);

    return (
        <View style={styles.container}>
            <Image
                style={styles.img}
                source={{ uri: `${API_URL}${item.picture}` }}
            />

            <View style={styles.containerName}>
                <Text>{item.name}</Text>
            </View>

            <View style={styles.containerDesc}>
                <Text>{item.description}</Text>
            </View>

            <Text>Height: {item.height}</Text>
            <Text>Weight: {item.weight}</Text>
            <Text>Type: {item.type}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
    },
    img: {
        height: 250,
        width: '100%',
        resizeMode: 'contain',
    },
    containerName: {
        paddingTop: 10,
        alignSelf: 'center',
    },
    containerDesc: {
        paddingTop: 10,
        paddingBottom: 10,
        alignSelf: 'center',
    },
});

export default observer(DetailScreen);
