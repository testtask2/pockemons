import React, { useContext } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import { observer } from 'mobx-react';
import { MainStoreContext } from '../store';
import ListItem from '../components/ListItem';

const FavScreen = ({ navigation }) => {
    const store = useContext(MainStoreContext);

    const toDetailScreen = (item) => {
        navigation.navigate('Detail', {
            item,
        });
    };

    return (
        <View style={styles.container}>
            <FlatList
                data={store.data.filter((i) => store.isFavorite(i.name))}
                keyExtractor={(item) => item.name.toString()}
                renderItem={({ item }) => (
                    <ListItem
                        item={item}
                        toDetailScreen={toDetailScreen}
                        isFavorite={store.isFavorite(item.name)}
                        addFavorite={store.addFavorite}
                        removeFavorite={store.removeFavorite}
                    />
                )}
                extraData={store.favorites.slice()}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default observer(FavScreen);
