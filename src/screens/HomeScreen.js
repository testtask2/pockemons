import React, { useContext } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import { observer } from 'mobx-react';
import { MainStoreContext } from '../store';
import Loader from '../components/Loader';
import ListItem from '../components/ListItem';

const HomeScreen = ({ navigation }) => {
    const store = useContext(MainStoreContext);

    if (store.isLoading) {
        return <Loader />;
    }

    const toDetailScreen = (item) => {
        navigation.navigate('Detail', {
            item,
        });
    };

    return (
        <View style={styles.container}>
            <FlatList
                data={store.data}
                keyExtractor={(item) => item.name.toString()}
                renderItem={({ item }) => (
                    <ListItem
                        item={item}
                        toDetailScreen={toDetailScreen}
                        isFavorite={store.isFavorite(item.name)}
                        addFavorite={store.addFavorite}
                        removeFavorite={store.removeFavorite}
                    />
                )}
                extraData={store.favorites.slice()}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default observer(HomeScreen);
