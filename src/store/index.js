import { makeAutoObservable } from 'mobx';
import { createContext } from 'react';
import { API_URL } from '../const';

class MainStore {
    data = [];
    isLoading = true;
    error = '';
    favorites = [];

    constructor() {
        makeAutoObservable(this);
        this.getPockemons();
    }

    getPockemons = async () => {
        this.isLoading = true;

        try {
            const response = await fetch(`${API_URL}/pockemons/data.json`);
            const responseJson = await response.json();
            this.data.replace(responseJson);
        } catch (error) {
            this.error = error;
        }

        this.isLoading = false;
    }

    addFavorite = (name) => {
        this.favorites.push(name);
    }

    removeFavorite = (name) => {
        this.favorites.remove(name);
    }

    isFavorite = (name) => {
        return this.favorites.includes(name);
    }
}

export const mainStore = new MainStore();
export const MainStoreContext = createContext(mainStore);
